# README #

## WARNING
This repository has been moved to https://github.com/psugrg/arm-m-ridiculus-startup and will no longer be maintained here!


### Ridiculously small sartup code for the ARM Cortex-M processors family

### What is this repository for? ###

The project intention is to demonstrate how to create ridiculously small startup code for the ARM Cortex-M processors that contains only the absolute minimum of code. The main requirement was to use C language and to make the build system to not to add any extra code (libraries, startup code etc.). 

### What is this repository _not_ for? ###

This project cannot be used for any standard development activities since it doesn't setup the complete environment whatsoever.

### Usage ###

Use standard `make all` and `make clean` commands to compile and clean the code.
